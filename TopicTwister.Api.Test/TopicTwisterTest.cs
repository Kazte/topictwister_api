using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;
using TopicTwister.Api.Controllers;
using TopicTwister.Api.Interfaces;
using Moq;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.InputObjects;
using TopicTwister.Api.OutputObjects;
using TopicTwister.Api.Providers;
using TopicTwister.Api.Rounds.Mocks;
using TopicTwister.Api.Services;
using TopicTwister.Api.Use_Cases;
using Match = TopicTwister.Api.Domain.Models.Match;

namespace TopicTwister.Api.Test;

public class TopicTwisterTest
{
    // [Fact]
    // public async Task Get_OnSuccess_ReturnStatusCode200()
    // {
    //     // Arrange
    //     var mockTopicTwisterService = new Mock<ITopicTwisterContext>();
    //
    //     var controller = new TopicTwisterController(mockTopicTwisterService.Object);
    //
    //     // Act
    //     var result = await controller.Get() as OkObjectResult;
    //
    //     // Assert
    //     result.StatusCode.Should().Be(200);
    // }
    //
    // [Fact]
    // public async Task Get_OnSuccess_ReturnListOfMatches()
    // {
    //     // Arrange
    //     var mockTopicTwisterContext = new Mock<ITopicTwisterContext>();
    //     mockTopicTwisterContext
    //         .Setup(ctx => ctx.GetAllMatches())
    //         .ReturnsAsync(new List<Match>());
    //
    //     var controller = new TopicTwisterController(mockTopicTwisterContext.Object);
    //
    //     // Act
    //     var res = await controller.Get();
    //
    //     // Assert
    //     res.Should().BeOfType<OkObjectResult>();
    //     var objectResult = (OkObjectResult)res;
    //     objectResult.Value.Should().BeOfType<List<Match>>();
    // }

    [Fact]
    public async Task Get_OnSuccess_GetRandomLetter_NotNull()
    {
        // Arrange
        var context = new Mock<ITopicTwisterRepository>();
        var letters = new List<Letter>
        {
            new Letter() { Description = "A", IdLetter = 0 }
        };
        context.Setup(ctx => ctx.GetLetters()).ReturnsAsync(letters);

        var actionGetRandomLetter = LetterActionProvider.GetRandomLetter(context.Object);

        // Act
        var randomLetter = await actionGetRandomLetter.Execute();

        // Assert
        randomLetter.Should().BeOfType<Letter>();
    }

    [Fact]
    public async Task Get_OnSuccess_GetRandomLetter()
    {
        // Arrange
        var context = new Mock<ITopicTwisterRepository>();

        var lettersList = new List<Letter>
        {
            new Letter() { Description = "A", IdLetter = 0 },
            new Letter() { Description = "B", IdLetter = 1 },
            new Letter() { Description = "C", IdLetter = 2 }
        };

        context.Setup(ctx => ctx.GetLetters()).ReturnsAsync(lettersList);

        var actionGetRandomLetter = LetterActionProvider.GetRandomLetter(context.Object);
        var actionGetLetters = LetterActionProvider.GetLetters(context.Object);

        // Act
        var letters = await actionGetLetters.Execute();
        var randomLetter = await actionGetRandomLetter.Execute();

        // Assert
        letters.Should().Contain(x => x.IdLetter == randomLetter.IdLetter);
    }

    [Fact]
    public async Task Get_OnSuccess_GetRandomCategory_NotNull()
    {
        // Arrange
        var context = new Mock<ITopicTwisterRepository>();

        var categories = new List<Category>
        {
            new Category { CategoryName = "Frutas" },
            new Category { CategoryName = "Nombres" },
        };

        context.Setup(ctx => ctx.GetCategories()).ReturnsAsync(categories);

        var actionGetRandomCategory = CategoryActionProvider.GetRandomCategory(context.Object);

        // Act
        var randomCategory = await actionGetRandomCategory.Execute();

        // Assert
        randomCategory.Should().BeOfType<Category>();
    }

    [Fact]
    public async Task Get_OnSuccess_GetRandomCategory()
    {
        // Arrange
        var context = new Mock<ITopicTwisterRepository>();

        var categoriesList = new List<Category>
        {
            new Category { CategoryName = "Frutas" },
            new Category { CategoryName = "Nombres" },
        };

        context.Setup(ctx => ctx.GetCategories()).ReturnsAsync(categoriesList);


        var actionGetRandomCategory = CategoryActionProvider.GetRandomCategory(context.Object);
        var actionGetCategories = CategoryActionProvider.GetCategories(context.Object);

        // Act
        var categories = await actionGetCategories.Execute();
        var randomCategory = await actionGetRandomCategory.Execute();

        // Assert
        categories.Should().Contain(x => x.IdCategory == randomCategory.IdCategory);
    }

    [Fact]
    public async Task Validator_CheckIfWordIsInCategory()
    {
        // Arrange
        var word = "Anana";
        var categoryName = "Frutas y Verduras";

        var categoriesList = new List<Category>
        {
            new Category { CategoryName = "Frutas y Verduras", IdCategory = 1 },
            new Category { CategoryName = "Nombres", IdCategory = 2 },
        };

        var wordsList = new List<Word>
        {
            new Word { Description = "Anana", IdCategory = 1 },
            new Word { Description = "Ana", IdCategory = 2 }
        };

        var context = new Mock<ITopicTwisterRepository>();

        context.Setup(ctx => ctx.GetCategories()).ReturnsAsync(categoriesList);
        context.Setup(ctx => ctx.GetWords()).ReturnsAsync(wordsList);

        var actionIsWordInCategory = ValidatorActionProvider.IsWordInCategory(context.Object);

        // Act
        var isWordInCategory = await actionIsWordInCategory.Execute(categoryName, word);

        // Assert
        isWordInCategory.Should().Be(true);
    }

    [Fact]
    public async Task Validator_CheckIfWordIsNotInCategory()
    {
        // Arrange
        var word = "Pantalon";
        var categoryName = "Frutas y Verduras";

        var categoriesList = new List<Category>
        {
            new Category { CategoryName = "Frutas y Verduras", IdCategory = 1 },
            new Category { CategoryName = "Nombres", IdCategory = 2 },
        };

        var wordsList = new List<Word>
        {
            new Word { Description = "Anana", IdCategory = 1 },
            new Word { Description = "Ana", IdCategory = 2 }
        };

        var context = new Mock<ITopicTwisterRepository>();

        context.Setup(ctx => ctx.GetCategories()).ReturnsAsync(categoriesList);
        context.Setup(ctx => ctx.GetWords()).ReturnsAsync(wordsList);

        var actionIsWordInCategory = ValidatorActionProvider.IsWordInCategory(context.Object);

        // Act
        var isWordInCategory = await actionIsWordInCategory.Execute(categoryName, word);

        // Assert
        isWordInCategory.Should().Be(false);
    }

    [Fact]
    public async Task Validator_CheckIfWordIsInCategoryAndIsRight()
    {
        // Arrange
        var word = "Anana";
        var categoryName = "Frutas y Verduras";
        var letter = "A";

        var categoriesList = new List<Category>
        {
            new Category { CategoryName = "Frutas y Verduras", IdCategory = 1 },
            new Category { CategoryName = "Nombres", IdCategory = 2 },
        };

        var wordsList = new List<Word>
        {
            new Word { Description = "Anana", IdCategory = 1 },
            new Word { Description = "Ana", IdCategory = 2 }
        };

        var context = new Mock<ITopicTwisterRepository>();

        context.Setup(ctx => ctx.GetCategories()).ReturnsAsync(categoriesList);
        context.Setup(ctx => ctx.GetWords()).ReturnsAsync(wordsList);

        var actionValidateWord = ValidatorActionProvider.ValidateWord(context.Object);

        // Act
        var isRight = await actionValidateWord.Execute(categoryName, letter, word);

        // Assert
        isRight.Should().Be(true);
    }

    [Fact]
    public async Task Validator_CheckIfWordIsInCategoryAndIsWrongWord()
    {
        // Arrange
        var word = "Pantalon";
        var categoryName = "Frutas y Verduras";
        var letter = "A";

        var context = new Mock<ITopicTwisterRepository>();

        var actionValidateWord = ValidatorActionProvider.ValidateWord(context.Object);

        // Act
        var isRight = await actionValidateWord.Execute(categoryName, letter, word);

        // Assert
        isRight.Should().Be(false);
    }

    [Fact]
    public async Task Validator_CheckIfWordIsInCategoryAndIsWrongLetter()
    {
        // Arrange
        var word = "Anana";
        var categoryName = "Frutas y Verduras";
        var letter = "P";

        var context = new Mock<ITopicTwisterRepository>();

        var actionValidateWord = ValidatorActionProvider.ValidateWord(context.Object);

        // Act
        var isRight = await actionValidateWord.Execute(categoryName, letter, word);

        // Assert
        isRight.Should().Be(false);
    }

    [Fact]
    public async Task Category_GetDesiredAmountOfCategories()
    {
        // Arrange
        var amountOfCategories = 5;

        var context = new Mock<ITopicTwisterRepository>();

        var categoriesList = new List<Category>
        {
            new Category { CategoryName = "Frutas y Verduras", IdCategory = 1 },
            new Category { CategoryName = "Nombres", IdCategory = 2 },
            new Category { CategoryName = "Objectos", IdCategory = 3 },
            new Category { CategoryName = "Paises", IdCategory = 4 },
            new Category { CategoryName = "Idiomas", IdCategory = 5 },
            new Category { CategoryName = "Colores", IdCategory = 6 },
        };

        context.Setup(ctx => ctx.GetCategories()).ReturnsAsync(categoriesList);


        // Act
        var categories = await CategoryActionProvider.GetMultipleCategories(context.Object).Execute(amountOfCategories);

        // Assert
        categories.Should().HaveCount(amountOfCategories);
    }

    [Fact]
    public async Task Round_GetCurrentRound()
    {
        // Arrange

        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();

        var match = new MatchMockGenerator().GenerateMockupMatch_TwoRounds();

        mockupTopicTwisterContext.Setup(ctx => ctx.GetRoundsByMatchId(match.IdMatch))
            .ReturnsAsync(match.Rounds.ToList());

        // Act
        var currentRoundId =
            await RoundActionProvider.GetCurrentRoundId(mockupTopicTwisterContext.Object).Execute(match);

        // Assert
        currentRoundId.Should().Be(2);
    }

    [Fact]
    public async Task Round_CheckForWinner_PlayerOne()
    {
        // Arrange
        var answers = new MockupAnswers().GetAnswersPlayerOneWin();
        var match = new MatchMockGenerator().GenerateMockupMatch(answers);
        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();

        mockupTopicTwisterContext.Setup(ctx => ctx.GetAnswersByRoundId(1)).ReturnsAsync(answers);
        mockupTopicTwisterContext.Setup(ctx => ctx.GetRoundsByMatchId(match.IdMatch))
            .ReturnsAsync(match.Rounds.ToList());

        // Act
        var newMatch = await RoundActionProvider.CheckWinner(mockupTopicTwisterContext.Object).Execute(match);


        // Assert
        newMatch.UserOneScore.Should().Be(1);
        newMatch.UserTwoScore.Should().Be(0);
    }

    [Fact]
    public async Task Round_CheckForWinner_PlayerTwo()
    {
        // Arrange
        var answers = new MockupAnswers().GetAnswersPlayerTwoWin();
        var match = new MatchMockGenerator().GenerateMockupMatch(answers);
        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();

        mockupTopicTwisterContext.Setup(ctx => ctx.GetAnswersByRoundId(1)).ReturnsAsync(answers);
        mockupTopicTwisterContext.Setup(ctx => ctx.GetRoundsByMatchId(match.IdMatch))
            .ReturnsAsync(match.Rounds.ToList());

        // Act
        var newMatch = await RoundActionProvider.CheckWinner(mockupTopicTwisterContext.Object).Execute(match);


        // Assert
        newMatch.UserOneScore.Should().Be(0);
        newMatch.UserTwoScore.Should().Be(1);
    }

    [Fact]
    public async Task Round_CheckForTie()
    {
        // Arrange
        var answers = new MockupAnswers().GetAnswersTie();
        var match = new MatchMockGenerator().GenerateMockupMatch(answers);
        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();

        mockupTopicTwisterContext.Setup(ctx => ctx.GetAnswersByRoundId(1)).ReturnsAsync(answers);
        mockupTopicTwisterContext.Setup(ctx => ctx.GetRoundsByMatchId(match.IdMatch))
            .ReturnsAsync(match.Rounds.ToList());

        // Act
        var newMatch = await RoundActionProvider.CheckWinner(mockupTopicTwisterContext.Object).Execute(match);


        // Assert
        newMatch.UserOneScore.Should().Be(0);
        newMatch.UserTwoScore.Should().Be(0);
    }

    [Fact]
    public async Task UseCase_FindMatchByPrimaryCriteria()
    {
        // Los dos usuarios tienen el mismo número de victorias +/- 5.

        // Arrange
        var usersList = new List<UserOutput>()
        {
            new UserOutput()
            {
                userId = "1",
                username = "Quark",
                wins = 2,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "2",
                username = "Franco",
                wins = 3,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "3",
                username = "Bruno",
                wins = 10,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "4",
                username = "Eduardo",
                wins = 2,
                isFinding = false
            },
        };

        var mockUserService = new Mock<IUserService>();
        mockUserService
            .Setup(ctx => ctx.GetUsers())
            .ReturnsAsync(usersList);

        mockUserService
            .Setup(ctx => ctx.GetUserById("1"))
            .ReturnsAsync(usersList.Find(x => x.userId == "1"));

        var mockTopicTwisterRepository = new Mock<ITopicTwisterRepository>();

        var matches = new List<Domain.Models.Match>();

        mockTopicTwisterRepository
            .Setup(ctx => ctx.GetMatchesByUserId("1"))
            .ReturnsAsync(matches);

        var findMatchUseCase = new FindMatchUseCase(mockUserService.Object, mockTopicTwisterRepository.Object);

        // Act
        var matchFound = await findMatchUseCase.FindMatchWithId("1");

        // Assert
        matchFound.Should().BeOfType(typeof(Domain.Models.Match));
        matchFound.IdUserOne.Should().Be("1");
        matchFound.IdUserTwo.Should().Be("2");
    }

    [Fact]
    public async Task UseCase_FindMatchBySecondaryCriteria()
    {
        // Si no se encuentra un usuario que está esperando por una partida en base al criterio 1,
        // entonces se puede tomar un usuario con menos de 5 victorias.

        // Arrange
        var usersList = new List<UserOutput>()
        {
            new UserOutput()
            {
                userId = "1",
                username = "Quark",
                wins = 4,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "2",
                username = "Franco",
                wins = 15,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "3",
                username = "Bruno",
                wins = 1,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "4",
                username = "Eduardo",
                wins = 2,
                isFinding = false
            },
        };

        var mockUserService = new Mock<IUserService>();
        mockUserService
            .Setup(ctx => ctx.GetUsers())
            .ReturnsAsync(usersList);

        mockUserService
            .Setup(ctx => ctx.GetUserById("1"))
            .ReturnsAsync(usersList.Find(x => x.userId == "1"));

        var mockTopicTwisterRepository = new Mock<ITopicTwisterRepository>();

        var matches = new List<Domain.Models.Match>();

        mockTopicTwisterRepository
            .Setup(ctx => ctx.GetMatchesByUserId("1"))
            .ReturnsAsync(matches);

        var findMatchUseCase = new FindMatchUseCase(mockUserService.Object, mockTopicTwisterRepository.Object);

        // Act
        var matchFound = await findMatchUseCase.FindMatchWithId("1");

        // Assert
        matchFound.Should().BeOfType(typeof(Domain.Models.Match));
        matchFound.IdUserOne.Should().Be("1");
        matchFound.IdUserTwo.Should().Be("3");
    }

    [Fact]
    public async Task UseCase_FindMatchByTertiaryCriteria()
    {
        // Si no se encuentra un usuario que está esperando por una partida en base al criterio 2,
        // entonces se puede tomar un usuario de manera aleatoria.

        // Arrange
        var usersList = new List<UserOutput>()
        {
            new UserOutput()
            {
                userId = "1",
                username = "Quark",
                wins = 4,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "2",
                username = "Franco",
                wins = 15,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "3",
                username = "Bruno",
                wins = 16,
                isFinding = false
            },
            new UserOutput()
            {
                userId = "4",
                username = "Eduardo",
                wins = 2,
                isFinding = false
            },
        };

        var mockUserService = new Mock<IUserService>();
        mockUserService
            .Setup(ctx => ctx.GetUsers())
            .ReturnsAsync(usersList);

        mockUserService
            .Setup(ctx => ctx.GetUserById("1"))
            .ReturnsAsync(usersList.Find(x => x.userId == "1"));

        var mockTopicTwisterRepository = new Mock<ITopicTwisterRepository>();

        var matches = new List<Domain.Models.Match>();

        mockTopicTwisterRepository
            .Setup(ctx => ctx.GetMatchesByUserId("1"))
            .ReturnsAsync(matches);

        var findMatchUseCase = new FindMatchUseCase(mockUserService.Object, mockTopicTwisterRepository.Object);

        // Act
        var matchFound = await findMatchUseCase.FindMatchWithId("1");

        // Assert
        matchFound.Should().BeOfType(typeof(Domain.Models.Match));
        matchFound.IdUserOne.Should().Be("1");
        matchFound.IdUserTwo.Should().Be("2");
    }

    [Fact]
    public async Task UseCase_FindMatch_WithCurrentMatchActiveOpponent()
    {
        // Arrange
        var usersList = new List<UserOutput>()
        {
            new UserOutput()
            {
                userId = "1",
                username = "Quark",
                wins = 4,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "2",
                username = "Franco",
                wins = 4,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "3",
                username = "Bruno",
                wins = 4,
                isFinding = false
            },
            new UserOutput()
            {
                userId = "4",
                username = "Eduardo",
                wins = 4,
                isFinding = false
            },
        };

        var mockUserService = new Mock<IUserService>();
        mockUserService
            .Setup(ctx => ctx.GetUsers())
            .ReturnsAsync(usersList);

        mockUserService
            .Setup(ctx => ctx.GetUserById("1"))
            .ReturnsAsync(usersList.Find(x => x.userId == "1"));

        var mockTopicTwisterRepository = new Mock<ITopicTwisterRepository>();

        var matches = new List<Domain.Models.Match>()
        {
            new Domain.Models.Match()
            {
                IdUserOne = "1",
                IdUserTwo = "2",
                IsClosed = false
            }
        };

        mockTopicTwisterRepository
            .Setup(ctx => ctx.GetMatchesByUserId("1"))
            .ReturnsAsync(matches);

        var findMatchUseCase = new FindMatchUseCase(mockUserService.Object, mockTopicTwisterRepository.Object);

        // Act
        var matchFound = await findMatchUseCase.FindMatchWithId("1");

        // Assert
        matchFound.Should().BeNull();
    }

    [Fact]
    public async Task UseCase_FindMatch_WithoutCurrentMatchActiveOpponent()
    {
        // Arrange
        var usersList = new List<UserOutput>()
        {
            new UserOutput()
            {
                userId = "1",
                username = "Quark",
                wins = 4,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "2",
                username = "Franco",
                wins = 4,
                isFinding = true
            },
            new UserOutput()
            {
                userId = "3",
                username = "Bruno",
                wins = 4,
                isFinding = false
            },
            new UserOutput()
            {
                userId = "4",
                username = "Eduardo",
                wins = 4,
                isFinding = false
            },
        };

        var mockUserService = new Mock<IUserService>();
        mockUserService
            .Setup(ctx => ctx.GetUsers())
            .ReturnsAsync(usersList);

        mockUserService
            .Setup(ctx => ctx.GetUserById("1"))
            .ReturnsAsync(usersList.Find(x => x.userId == "1"));

        var mockTopicTwisterRepository = new Mock<ITopicTwisterRepository>();

        var matches = new List<Domain.Models.Match>()
        {
            new Domain.Models.Match()
            {
                IdUserOne = "1",
                IdUserTwo = "3",
                IsClosed = false
            }
        };

        mockTopicTwisterRepository
            .Setup(ctx => ctx.GetMatchesByUserId("1"))
            .ReturnsAsync(matches);

        var findMatchUseCase = new FindMatchUseCase(mockUserService.Object, mockTopicTwisterRepository.Object);

        // Act
        var matchFound = await findMatchUseCase.FindMatchWithId("1");

        // Assert
        matchFound.Should().NotBeNull();
        matchFound.Should().BeOfType(typeof(Domain.Models.Match));
        matchFound.IdUserOne.Should().Be("1");
        matchFound.IdUserTwo.Should().Be("2");
    }


    [Fact]
    public async Task UseCase_CategoryGenerator_CategoriesGeneratedAreInsideTheArray()
    {
        // Arrange
        List<Category> categories = new List<Category>()
        {
            new Category()
            {
                IdCategory = 1,
                CategoryName = "Frutas y Verduras"
            },
            new Category()
            {
                IdCategory = 2,
                CategoryName = "Objetos"
            },
            new Category()
            {
                IdCategory = 3,
                CategoryName = "Paises"
            },
            new Category()
            {
                IdCategory = 4,
                CategoryName = "Profesiones"
            },
            new Category()
            {
                IdCategory = 5,
                CategoryName = "Lenguajes"
            },
        };

        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();
        mockupTopicTwisterContext.Setup(ctx => ctx.GetCategories()).ReturnsAsync(categories);

        var categoryGenerator = new CategoriesGeneratorUserCase(mockupTopicTwisterContext.Object);

        // Act
        var categoriesGenerated = await categoryGenerator.GenerateCategories(3);
        var categoriesNames = categoriesGenerated.Select(cat => cat.CategoryName).ToList();

        // Assert
        foreach (var category in categoriesGenerated)
        {
            Assert.Contains(category.CategoryName, categoriesNames);
        }
    }

    [Fact]
    public async Task UseCase_LettersGenerator_LettersGeneratedAreInsideTheArray()
    {
        // Arrange
        List<Letter> letters = new List<Letter>()
        {
            new Letter()
            {
                IdLetter = 1,
                Description = "A"
            },
            new Letter()
            {
                IdLetter = 2,
                Description = "B"
            },
            new Letter()
            {
                IdLetter = 3,
                Description = "C"
            },
            new Letter()
            {
                IdLetter = 4,
                Description = "D"
            }
        };

        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();
        mockupTopicTwisterContext.Setup(ctx => ctx.GetLetters()).ReturnsAsync(letters);

        var lettersGenerator = new LetterGeneratorUseCase(mockupTopicTwisterContext.Object);

        // Act
        var letterGenerated = await lettersGenerator.GenerateLetter();

        // Assert
        letters.Should().Contain(letterGenerated);
    }

    [Fact]
    public async Task UseCase_RoundGenerator()
    {
        // Arrange
        List<Category> categories = new List<Category>()
        {
            new Category()
            {
                IdCategory = 1,
                CategoryName = "Frutas y Verduras"
            },
            new Category()
            {
                IdCategory = 2,
                CategoryName = "Objetos"
            },
            new Category()
            {
                IdCategory = 3,
                CategoryName = "Paises"
            },
            new Category()
            {
                IdCategory = 4,
                CategoryName = "Profesiones"
            },
            new Category()
            {
                IdCategory = 5,
                CategoryName = "Lenguajes"
            },
        };

        List<Word> words = new List<Word>()
        {
            new Word()
            {
                IdWord = 1,
                Description = "Anana",
                IdCategory = 1
            },
            new Word()
            {
                IdWord = 2,
                Description = "Banana",
                IdCategory = 1
            },
            new Word()
            {
                IdWord = 3,
                Description = "Coco",
                IdCategory = 1
            },
            new Word()
            {
                IdWord = 4,
                Description = "Arma",
                IdCategory = 2
            },
            new Word()
            {
                IdWord = 5,
                Description = "Bala",
                IdCategory = 2
            },
            new Word()
            {
                IdWord = 6,
                Description = "Cargador",
                IdCategory = 2
            },
            new Word()
            {
                IdWord = 7,
                Description = "Argentina",
                IdCategory = 3
            },
            new Word()
            {
                IdWord = 8,
                Description = "Bolivia",
                IdCategory = 3
            },
            new Word()
            {
                IdWord = 9,
                Description = "Colombia",
                IdCategory = 3
            },

            new Word()
            {
                IdWord = 10,
                Description = "Astronauta",
                IdCategory = 4
            },
            new Word()
            {
                IdWord = 11,
                Description = "Bombero",
                IdCategory = 4
            },
            new Word()
            {
                IdWord = 12,
                Description = "Carpintero",
                IdCategory = 4
            },

            new Word()
            {
                IdWord = 13,
                Description = "Aleman",
                IdCategory = 5
            },
            new Word()
            {
                IdWord = 14,
                Description = "Belga",
                IdCategory = 5
            },
            new Word()
            {
                IdWord = 15,
                Description = "Catalan",
                IdCategory = 5
            },
        };

        List<Letter> letters = new List<Letter>()
        {
            new Letter()
            {
                IdLetter = 1,
                Description = "A"
            },
            new Letter()
            {
                IdLetter = 2,
                Description = "B"
            },
            new Letter()
            {
                IdLetter = 3,
                Description = "C"
            }
        };

        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();
        mockupTopicTwisterContext.Setup(ctx => ctx.GetCategories()).ReturnsAsync(categories);
        mockupTopicTwisterContext.Setup(ctx => ctx.GetWords()).ReturnsAsync(words);
        mockupTopicTwisterContext.Setup(ctx => ctx.GetLetters()).ReturnsAsync(letters);

        var roundGeneratorUseCase = new RoundsGeneratorUseCase(mockupTopicTwisterContext.Object);

        var amountOfRounds = 3;

        // Act
        var roundsGenerated = await roundGeneratorUseCase.GenerateRoundsByAmount(amountOfRounds);

        // Assert
        roundsGenerated.Count.Should().Be(amountOfRounds);
    }

    [Fact]
    public async Task UseCase_EndMatch_PlayerOneWin()
    {
        // Arrange
        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();
        mockupTopicTwisterContext.Setup(ctx => ctx.GetMatchById(1))
            .ReturnsAsync(new MatchMockGenerator().GenerateMockupMatch_FullRounds_PlayerOneWin);

        var endMatchUseCase = new EndMatchUseCase(mockupTopicTwisterContext.Object);

        // Act
        var matchClosed = await endMatchUseCase.EndMatch(1);

        // Assert
        matchClosed.IsClosed.Should().Be(true);
        matchClosed.IdMatchStatus.Should().Be(3);
    }

    [Fact]
    public async Task UseCase_EndMatch_PlayerTwoWin()
    {
        // Arrange
        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();
        mockupTopicTwisterContext.Setup(ctx => ctx.GetMatchById(1))
            .ReturnsAsync(new MatchMockGenerator().GenerateMockupMatch_FullRounds_PlayerTwoWin);

        var endMatchUseCase = new EndMatchUseCase(mockupTopicTwisterContext.Object);

        // Act
        var matchClosed = await endMatchUseCase.EndMatch(1);

        // Assert
        matchClosed.IsClosed.Should().Be(true);
        matchClosed.IdMatchStatus.Should().Be(4);
    }

    [Fact]
    public async Task UseCase_EndMatch_Tie()
    {
        // Arrange
        var mockupTopicTwisterContext = new Mock<ITopicTwisterRepository>();
        mockupTopicTwisterContext.Setup(ctx => ctx.GetMatchById(1))
            .ReturnsAsync(new MatchMockGenerator().GenerateMockupMatch_FullRounds_Tie);

        var endMatchUseCase = new EndMatchUseCase(mockupTopicTwisterContext.Object);

        // Act
        var matchClosed = await endMatchUseCase.EndMatch(1);

        // Assert
        matchClosed.IsClosed.Should().Be(true);
        matchClosed.IdMatchStatus.Should().Be(5);
    }

    [Fact]
    public async Task Match_CheckIfAllRoundsAreClosed()
    {
        // Arrange
        var match = new MatchMockGenerator().GenerateMockupMatch_FullRounds_Tie();

        var actionIsAllRoundsClosed = MatchActionProvider.IsAllRoundsClosed();

        // Act
        var isAllRoundsClosed = await actionIsAllRoundsClosed.Execute(match);

        // Assert
        isAllRoundsClosed.Should().Be(true);
    }

    [Fact]
    public async Task Match_CheckIfAllRoundsAreClosed_False()
    {
        // Arrange
        var match = new MatchMockGenerator().GenerateMockupMatch_TwoRounds();

        var actionIsAllRoundsClosed = MatchActionProvider.IsAllRoundsClosed();

        // Act
        var isAllRoundsClosed = await actionIsAllRoundsClosed.Execute(match);

        // Assert
        isAllRoundsClosed.Should().Be(false);
    }
}