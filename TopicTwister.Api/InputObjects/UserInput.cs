﻿namespace TopicTwister.Api.InputObjects;

public class UserInput
{
    public int UserId { get; set; }
    public string Username { get; set; }
    public bool IsFinding { get; set; }
    public int Wins { get; set; }
}