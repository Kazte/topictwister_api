﻿namespace TopicTwister.Api.InputObjects;

public class UserValidationInput
{
    public string Email { get; set; }
    public string Password { get; set; }
}