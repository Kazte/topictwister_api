﻿namespace TopicTwister.Api.InputObjects;

public class AnswerInput
{
    public int CategoryId { get; set; }
    public string Answer { get; set; }
    public string UserId { get; set; }
    public int RoundId { get; set; }
}