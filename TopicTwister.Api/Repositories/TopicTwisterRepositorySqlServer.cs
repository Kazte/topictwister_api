﻿using Microsoft.EntityFrameworkCore;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.InputObjects;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.Providers;

namespace TopicTwister.Api.Repositories;

public class TopicTwisterRepositorySqlServer : ITopicTwisterRepository
{
    private TopicTwisterContext _context;

    public TopicTwisterRepositorySqlServer(TopicTwisterContext context)
    {
        _context = context;
    }

    public async Task<List<Category>> GetCategories()
    {
        return await _context.Categories.ToListAsync();
    }

    public async Task<List<Word>> GetWords()
    {
        return await _context.Words.ToListAsync();
    }

    public async Task<List<Letter>> GetLetters()
    {
        return await _context.Letters.ToListAsync();
    }

    public async Task<int> AddMatch(Domain.Models.Match matchToAdd)
    {
        await using (var dbContextTransaction = await _context.Database.BeginTransactionAsync())
        {
            var matchAdded = await _context.Matches.AddAsync(matchToAdd);
            await _context.SaveChangesAsync();

            await dbContextTransaction.CommitAsync();

            return matchAdded.Entity.IdMatch;
        }
    }

    public async Task<int> AddRound(Round round)
    {
        await using (var dbContextTransaction = await _context.Database.BeginTransactionAsync())
        {
            var roundGenerated = await _context.Rounds.AddAsync(round);
            await _context.SaveChangesAsync();

            await dbContextTransaction.CommitAsync();

            return roundGenerated.Entity.IdRound;
        }
    }

    public async Task AddRoundCategory(RoundsCategory newRoundCategory)
    {
        await using (var dbContextTransaction = await _context.Database.BeginTransactionAsync())
        {
            await _context.RoundsCategories.AddAsync(newRoundCategory);

            await _context.SaveChangesAsync();

            await dbContextTransaction.CommitAsync();
        }
    }

    public async Task<Domain.Models.Match> GetMatchById(int matchId)
    {
        return await _context.Matches.FirstOrDefaultAsync(x => x.IdMatch == matchId);
    }

    public async Task<List<Round>> GetRoundsByMatchId(int matchId)
    {
        return await _context.Rounds.Where(x => x.IdMatch == matchId).ToListAsync();
    }

    public async Task<List<Answer>> GetAnswersByRoundId(int roundId)
    {
        return await _context.Answers.Where(x => x.IdRound == roundId).ToListAsync();
    }

    public async Task<List<Answer>> AddAnswers(List<AnswerInput> answerInputs)
    {
        await using (var dbContextTransaction = await _context.Database.BeginTransactionAsync())
        {
            var answersOutputList = new List<Answer>();

            var validateWordAction = ValidatorActionProvider.ValidateWord(this);

            foreach (var answerInput in answerInputs)
            {
                var answerEntity = new Answer();
                answerEntity.IdCategory = answerInput.CategoryId;
                answerEntity.IdRound = answerInput.RoundId;
                answerEntity.IdUser = answerInput.UserId;


                var category =
                    await _context.Categories.FirstOrDefaultAsync(x => x.IdCategory == answerInput.CategoryId);
                var round = await _context.Rounds.FirstOrDefaultAsync(x => x.IdRound == answerInput.RoundId);
                var letter = await _context.Letters.FirstOrDefaultAsync(x => x.IdLetter == round.IdLetter);

                answerEntity.IsRight =
                    await validateWordAction.Execute(category.CategoryName, letter.Description, answerInput.Answer);

                answerEntity.UserAnswer = answerInput.Answer;

                var answer = await _context.Answers.AddAsync(answerEntity);
                await _context.SaveChangesAsync();

                answersOutputList.Add(answer.Entity);
            }

            await dbContextTransaction.CommitAsync();

            return answersOutputList;
        }
    }

    public async Task<Round> GetRoundById(int roundId)
    {
        return await _context.Rounds.FirstOrDefaultAsync(x => x.IdRound == roundId);
    }

    public async Task UpdateMatchStatus(Domain.Models.Match match, int newMatchStatusId)
    {
        await using (var dbContextTransaction = await _context.Database.BeginTransactionAsync())
        {
            match.IdMatchStatus = newMatchStatusId;
            _context.Matches.Update(match);

            await _context.SaveChangesAsync();

            await dbContextTransaction.CommitAsync();
        }
    }

    public async Task UpdateCloseRound(Round round)
    {
        await using (var dbContextTransaction = await _context.Database.BeginTransactionAsync())
        {
            round.IsRoundClosed = true;
            _context.Rounds.Update(round);
            await _context.SaveChangesAsync();

            await dbContextTransaction.CommitAsync();
        }
    }

    public async Task<Domain.Models.Match> UpdateMatch(Domain.Models.Match newMatch)
    {
        await using (var dbContextTransaction = await _context.Database.BeginTransactionAsync())
        {
            var matchUpdated = _context.Matches.Update(newMatch);
            await _context.SaveChangesAsync();

            await dbContextTransaction.CommitAsync();

            return matchUpdated.Entity;
        }
    }

    public async Task<Domain.Models.Match> GetLastMatchByUserId(string userId)
    {
        var match = _context.Matches.OrderByDescending(x => x.IdMatch).FirstOrDefault(x => x.IdUserTwo == userId);

        return match;
    }

    public async Task<Category> GetCategoryById(int? id)
    {
        var category = await _context.Categories.FirstOrDefaultAsync(x => x.IdCategory == id);

        return category;
    }

    public async Task<List<RoundsCategory>> GetRoundsCategoryByRoundId(int roundIdRound)
    {
        var roundsCategory = await _context.RoundsCategories.Where(x => x.IdRound == roundIdRound).ToListAsync();

        return roundsCategory;
    }

    public async Task<Letter> GetLetterById(int id)
    {
        var letter = await _context.Letters.FirstOrDefaultAsync(x => x.IdLetter == id);

        return letter;
    }

    public async Task<List<Domain.Models.Match>> GetMatchesByUserId(string userId)
    {
        var matches = await _context.Matches.Where(x => x.IdUserOne == userId || x.IdUserTwo == userId).ToListAsync();

        return matches;
    }
}