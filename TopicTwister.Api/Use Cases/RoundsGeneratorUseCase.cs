﻿using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Use_Cases;

public class RoundsGeneratorUseCase
{
    private readonly ITopicTwisterRepository _repository;

    public RoundsGeneratorUseCase(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    

    public async Task<List<Round>> GenerateRoundsByAmount(int amountOfRounds)
    {
        var letterGenerator = new LetterGeneratorUseCase(_repository);

        List<Letter> lettersUsed = new List<Letter>();

        List<Round> rounds = new List<Round>();

        int round = 1;
        
        

        while (lettersUsed.Count < amountOfRounds)
        {
            var letter = await letterGenerator.GenerateLetter();

            if (!lettersUsed.Contains(letter))
            {
                lettersUsed.Add(letter);

                

                var roundGenerated = new Round()
                {
                    RoundNumber = round++,
                    IdLetter = letter.IdLetter,
                    IsRoundClosed = false,
                };

                rounds.Add(roundGenerated);
            }
        }

        return rounds;
    }
}