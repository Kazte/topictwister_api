﻿using System.Collections;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Use_Cases;

public class LetterGeneratorUseCase
{
    private readonly ITopicTwisterRepository _repository;

    public LetterGeneratorUseCase(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<Letter> GenerateLetter()
    {
        var letters = await _repository.GetLetters();
        var randomIndex = new Random().Next(0, letters.Count);

        return letters[randomIndex];
    }
}