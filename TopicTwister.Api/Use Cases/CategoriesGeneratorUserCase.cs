﻿using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Use_Cases;

public class CategoriesGeneratorUserCase
{
    private readonly ITopicTwisterRepository _repository;

    public CategoriesGeneratorUserCase(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<List<Category>> GenerateCategories(int amount)
    {
        var categories = await _repository.GetCategories();
        var randomCategoriesSelected = new List<Category>();

        while (randomCategoriesSelected.Count < amount && randomCategoriesSelected.Count <= categories.Count)
        {
            var randomNumber = new Random().Next(0, categories.Count);
            var categorySelected = categories[randomNumber];

            if (!randomCategoriesSelected.Contains(categorySelected))
                randomCategoriesSelected.Add(categorySelected);
        }

        return randomCategoriesSelected;
    }
}