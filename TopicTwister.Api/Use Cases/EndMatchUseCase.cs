﻿using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Use_Cases;

public class EndMatchUseCase
{
    private readonly ITopicTwisterRepository _repository;

    public EndMatchUseCase(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }


    public async Task<Domain.Models.Match> EndMatch(int matchId)
    {
        var match = await _repository.GetMatchById(matchId);

        match.IsClosed = true;

        if (match.UserOneScore > match.UserTwoScore)
        {
            match.IdMatchStatus = 3;
        }
        else if (match.UserOneScore < match.UserTwoScore)
        {
            match.IdMatchStatus = 4;
        }
        else
        {
            match.IdMatchStatus = 5;
        }

        return match;
    }
}