﻿using TopicTwister.Api.Interfaces;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.OutputObjects;
using TopicTwister.Api.Services;

namespace TopicTwister.Api.Use_Cases;

public class FindMatchUseCase
{
    private readonly IUserService _userService;
    private readonly ITopicTwisterRepository _topicTwisterRepository;

    public FindMatchUseCase(IUserService userService, ITopicTwisterRepository topicTwisterRepository)
    {
        _userService = userService;
        _topicTwisterRepository = topicTwisterRepository;
    }

    public async Task<Domain.Models.Match> FindMatchWithId(string userId)
    {
        // Buscar oponente
        var userOne = (await _userService.GetUserById(userId));

        var users = await _userService.GetUsers();

        var playerMatches = await _topicTwisterRepository.GetMatchesByUserId(userId);

        playerMatches = playerMatches.FindAll(x => !x.IsClosed);

        var opponentsIds = playerMatches.Select(x => x.IdUserOne == userId ? x.IdUserTwo : x.IdUserOne).ToList();

        var usersFindingMatch =
            users.FindAll(x => x.isFinding && !string.Equals(x.userId, userId) && !opponentsIds.Contains(x.userId));

        if (usersFindingMatch.Count == 0)
            return null;

        UserOutput userTwo;

        var userFirstCriteria = usersFindingMatch.Find(x => Math.Abs(x.wins - userOne.wins) <= 5);

        var userSecondCriteria = usersFindingMatch.Find(x => x.wins < 5);

        if (userFirstCriteria != null)
        {
            userTwo = userFirstCriteria;
        }
        else if (userSecondCriteria != null)
        {
            userTwo = userSecondCriteria;
        }
        else
        {
            var randomNumber = new Random().Next(0, usersFindingMatch.Count);
            userTwo = usersFindingMatch[randomNumber];
        }

        // Crear el match
        var newMatch = new Domain.Models.Match();

        newMatch.IdUserOne = userOne.userId;
        newMatch.IdUserTwo = userTwo.userId;
        newMatch.IdMatchStatus = 1;

        userOne.isFinding = false;
        userTwo.isFinding = false;
        newMatch.CurrentRoundNumber = 1;

        await _userService.UpdateUser(userOne);
        await _userService.UpdateUser(userTwo);

        return newMatch;
    }
}