﻿namespace TopicTwister.Api.Interfaces;

// TODO: Args genericos
public interface IAction<T>
{
    T Execute();
}