﻿using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.InputObjects;
using TopicTwister.Api.OutputObjects;

namespace TopicTwister.Api.Interfaces;

public interface ITopicTwisterRepository
{
    Task<List<Category>> GetCategories();
    Task<List<Word>> GetWords();

    Task<List<Letter>> GetLetters();
    Task<int> AddMatch(Domain.Models.Match matchToAdd);
    Task<int> AddRound(Round round);
    Task AddRoundCategory(RoundsCategory newRoundCategory);
    Task<Domain.Models.Match> GetMatchById(int matchId);
    Task<List<Round>> GetRoundsByMatchId(int matchId);
    Task<List<Answer>> GetAnswersByRoundId(int roundId);
    Task<List<Answer>> AddAnswers(List<AnswerInput> answerInputs);
    Task<Round> GetRoundById(int roundId);
    Task UpdateMatchStatus(Domain.Models.Match match, int newMatchStatusId);
    Task UpdateCloseRound(Round round);
    Task<Domain.Models.Match> UpdateMatch(Domain.Models.Match newMatch);
    Task<Domain.Models.Match> GetLastMatchByUserId(string userId);
    Task<Category> GetCategoryById(int? id);
    Task<List<RoundsCategory>> GetRoundsCategoryByRoundId(int roundIdRound);
    Task<Letter> GetLetterById(int id);
    Task<List<Domain.Models.Match>> GetMatchesByUserId(string userId);
}