using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.InputObjects;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.Mappers;
using TopicTwister.Api.Match.Controllers;
using TopicTwister.Api.OutputObjects;
using TopicTwister.Api.Providers;
using TopicTwister.Api.Repositories;
using TopicTwister.Api.Services;
using TopicTwister.Api.Use_Cases;

namespace TopicTwister.Api.Answers.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswerController : ControllerBase
    {
        private readonly ITopicTwisterRepository _repository;
        private readonly UserService _userService;

        public AnswerController()
        {
            _repository = new TopicTwisterRepositorySqlServer(new TopicTwisterContext());
            _userService = new UserService();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<AnswerOutput>))]
        public async Task<IActionResult> GetAnswers(int matchId, int roundNumber)
        {
            var answersOutputList = new List<AnswerOutput>();

            var round =
                (await _repository.GetRoundsByMatchId(matchId)).FirstOrDefault(x => x.RoundNumber == roundNumber);

            var answers = (await _repository.GetAnswersByRoundId(round.IdRound));

            var answerMapper = new AnswerMapper(_repository);

            foreach (var answer in answers)
            {
                var answerOutput = await answerMapper.Mapper(answer);
                answersOutputList.Add(answerOutput);
            }

            return new OkObjectResult(answersOutputList);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(MatchOutput))]
        public async Task<IActionResult> PostAnswers(List<AnswerInput> answerInputs)
        {
            var round = await _repository.GetRoundById(answerInputs[0].RoundId);

            if (round == null)
            {
                NotFound();
            }

            var match = await _repository.GetMatchById(round.IdMatch);

            if ((match.IdMatchStatus == 1 && answerInputs[0].UserId == match.IdUserTwo) ||
                (match.IdMatchStatus == 2 && answerInputs[0].UserId == match.IdUserOne))
            {
                return new ObjectResult(new { message = "No es el turno del usuario." });
            }

            // var answers = await _context.GetAnswersByRoundId(answerInputs[0].RoundId);

            await _repository.AddAnswers(answerInputs);

            if (answerInputs[0].UserId == match.IdUserOne)
            {
                await _repository.UpdateMatchStatus(match, 2);
            }
            else if (answerInputs[0].UserId == match.IdUserTwo)
            {
                var newMatch = await RoundActionProvider.CheckWinner(_repository).Execute(match);

                match = await _repository.UpdateMatch(newMatch);

                // await _context.UpdateCloseRound(round);
                await _repository.UpdateMatchStatus(match, 1);
            }

            var matchMapper = new MatchMapper(_repository, _userService);
            match = await _repository.GetMatchById(match.IdMatch);

            MatchOutput matchOutput = await matchMapper.Mapper(match);

            return new OkObjectResult(matchOutput);
        }
    }
}