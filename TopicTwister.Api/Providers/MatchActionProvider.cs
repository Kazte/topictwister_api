﻿using TopicTwister.Api.Actions;

namespace TopicTwister.Api.Providers;

public class MatchActionProvider
{
    public static IsAllRoundsClosed IsAllRoundsClosed() => new IsAllRoundsClosed();
}