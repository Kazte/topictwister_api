﻿using TopicTwister.Api.Actions;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Providers;

public class RoundActionProvider
{
    public static GetCurrentRoundId GetCurrentRoundId(ITopicTwisterRepository repository) => new GetCurrentRoundId(repository);
    public static CheckWinner CheckWinner(ITopicTwisterRepository repository) => new CheckWinner(repository);
}