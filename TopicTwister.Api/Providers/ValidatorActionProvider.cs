﻿using TopicTwister.Api.Actions;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Providers;

public class ValidatorActionProvider
{
    public static ValidateWord ValidateWord(ITopicTwisterRepository repository) => new ValidateWord(repository);

    public static IsWordInCategory IsWordInCategory(ITopicTwisterRepository repository) => new IsWordInCategory(repository);
}