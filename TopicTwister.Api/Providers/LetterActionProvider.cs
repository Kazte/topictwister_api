﻿using TopicTwister.Api.Actions;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Providers;

public class LetterActionProvider
{
    public static GetRandomLetter GetRandomLetter(ITopicTwisterRepository repository) => new GetRandomLetter(repository);

    public static GetLetters GetLetters(ITopicTwisterRepository repository) => new GetLetters(repository);
}