﻿using TopicTwister.Api.Actions;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Providers;

public class CategoryActionProvider
{
    public static GetRandomCategory GetRandomCategory(ITopicTwisterRepository repository) => new GetRandomCategory(repository);

    public static GetCategories GetCategories(ITopicTwisterRepository repository) => new GetCategories(repository);

    public static GetMultipleCategories GetMultipleCategories(ITopicTwisterRepository repository) => new GetMultipleCategories(repository);
}