﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace TopicTwister.Api.Domain.Models;

public partial class TopicTwisterContext : DbContext
{
    public TopicTwisterContext()
    {
    }

    public TopicTwisterContext(DbContextOptions<TopicTwisterContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Answer> Answers { get; set; }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Letter> Letters { get; set; }

    public virtual DbSet<Match> Matches { get; set; }

    public virtual DbSet<MatchStatus> MatchStatuses { get; set; }

    public virtual DbSet<Round> Rounds { get; set; }

    public virtual DbSet<RoundsCategory> RoundsCategories { get; set; }

    public virtual DbSet<Word> Words { get; set; }

    
    /*
     * To protect potentially sensitive information in your connection string, you should move it out of source code.
     * You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration -
     * see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings,
     * see http://go.microsoft.com/fwlink/?LinkId=723263.
     */
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer(StringConnections.ProductionMSSQLServer);

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Answer>(entity =>
        {
            entity.HasKey(e => e.IdAnswer).HasName("PK__Answers__A9B6978EEFE97FB2");

            entity.Property(e => e.IdAnswer).HasColumnName("idAnswer");
            entity.Property(e => e.IdCategory).HasColumnName("idCategory");
            entity.Property(e => e.IdRound).HasColumnName("idRound");
            entity.Property(e => e.IdUser)
                .HasMaxLength(50)
                .HasColumnName("idUser");
            entity.Property(e => e.IsRight).HasColumnName("isRight");
            entity.Property(e => e.UserAnswer)
                .HasMaxLength(255)
                .HasColumnName("userAnswer");

            entity.HasOne(d => d.IdCategoryNavigation).WithMany(p => p.Answers)
                .HasForeignKey(d => d.IdCategory)
                .HasConstraintName("FK_Answers_Categories");

            entity.HasOne(d => d.IdRoundNavigation).WithMany(p => p.Answers)
                .HasForeignKey(d => d.IdRound)
                .HasConstraintName("FK_Answers_Rounds");
        });

        modelBuilder.Entity<Category>(entity =>
        {
            entity.HasKey(e => e.IdCategory).HasName("PK__Categori__79D361B6B1E13EAB");

            entity.Property(e => e.IdCategory).HasColumnName("idCategory");
            entity.Property(e => e.CategoryName)
                .HasMaxLength(255)
                .HasColumnName("categoryName");
        });

        modelBuilder.Entity<Letter>(entity =>
        {
            entity.HasKey(e => e.IdLetter);

            entity.Property(e => e.IdLetter).HasColumnName("idLetter");
            entity.Property(e => e.Description)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("description");
        });

        modelBuilder.Entity<Match>(entity =>
        {
            entity.HasKey(e => e.IdMatch).HasName("PK__Matches__044BF56BBC8E0C07");

            entity.Property(e => e.IdMatch).HasColumnName("idMatch");
            entity.Property(e => e.CurrentRoundNumber).HasColumnName("currentRoundNumber");
            entity.Property(e => e.IdMatchStatus).HasColumnName("idMatchStatus");
            entity.Property(e => e.IdUserOne)
                .HasMaxLength(50)
                .HasColumnName("idUserOne");
            entity.Property(e => e.IdUserTwo)
                .HasMaxLength(50)
                .HasColumnName("idUserTwo");
            entity.Property(e => e.IsClosed).HasColumnName("isClosed");
            entity.Property(e => e.UserOneScore).HasColumnName("userOneScore");
            entity.Property(e => e.UserTwoScore).HasColumnName("userTwoScore");

            entity.HasOne(d => d.IdMatchStatusNavigation).WithMany(p => p.Matches)
                .HasForeignKey(d => d.IdMatchStatus)
                .HasConstraintName("FK_Matches_MatchStatus");
        });

        modelBuilder.Entity<MatchStatus>(entity =>
        {
            entity.HasKey(e => e.IdMatchStatus).HasName("PK__MatchSta__31725833A7568EFE");

            entity.ToTable("MatchStatus");

            entity.Property(e => e.IdMatchStatus).HasColumnName("idMatchStatus");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
        });

        modelBuilder.Entity<Round>(entity =>
        {
            entity.HasKey(e => e.IdRound).HasName("PK__Rounds__F6C717FD2BB61F24");

            entity.Property(e => e.IdRound).HasColumnName("idRound");
            entity.Property(e => e.IdLetter).HasColumnName("idLetter");
            entity.Property(e => e.IdMatch).HasColumnName("idMatch");
            entity.Property(e => e.IdUserWinner)
                .HasMaxLength(50)
                .HasColumnName("idUserWinner");
            entity.Property(e => e.IsRoundClosed).HasColumnName("isRoundClosed");
            entity.Property(e => e.RoundNumber).HasColumnName("roundNumber");

            entity.HasOne(d => d.IdLetterNavigation).WithMany(p => p.Rounds)
                .HasForeignKey(d => d.IdLetter)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Rounds_Letters");

            entity.HasOne(d => d.IdMatchNavigation).WithMany(p => p.Rounds)
                .HasForeignKey(d => d.IdMatch)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Rounds_Matches");
        });

        modelBuilder.Entity<RoundsCategory>(entity =>
        {
            entity.HasKey(e => e.IdRoundCategory).HasName("PK__RoundsCa__1D9AA149B33461D0");

            entity.ToTable("RoundsCategory");

            entity.Property(e => e.IdRoundCategory).HasColumnName("idRoundCategory");
            entity.Property(e => e.IdCategory).HasColumnName("idCategory");
            entity.Property(e => e.IdRound).HasColumnName("idRound");

            entity.HasOne(d => d.IdCategoryNavigation).WithMany(p => p.RoundsCategories)
                .HasForeignKey(d => d.IdCategory)
                .HasConstraintName("FK_RoundsCategory_Categories");

            entity.HasOne(d => d.IdRoundNavigation).WithMany(p => p.RoundsCategories)
                .HasForeignKey(d => d.IdRound)
                .HasConstraintName("FK_RoundsCategory_Rounds");
        });

        modelBuilder.Entity<Word>(entity =>
        {
            entity.HasKey(e => e.IdWord).HasName("PK__Words__15C3E27D30CFE881");

            entity.Property(e => e.IdWord).HasColumnName("idWord");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
            entity.Property(e => e.IdCategory).HasColumnName("idCategory");

            entity.HasOne(d => d.IdCategoryNavigation).WithMany(p => p.Words)
                .HasForeignKey(d => d.IdCategory)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Words_Categories");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
