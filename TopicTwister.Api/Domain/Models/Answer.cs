﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api.Domain.Models;

public partial class Answer
{
    public int IdAnswer { get; set; }

    public string? UserAnswer { get; set; }

    public int? IdCategory { get; set; }

    public int? IdRound { get; set; }

    public bool? IsRight { get; set; }

    public string? IdUser { get; set; }

    public virtual Category? IdCategoryNavigation { get; set; }

    public virtual Round? IdRoundNavigation { get; set; }
}
