﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api.Domain.Models;

public partial class Category
{
    public int IdCategory { get; set; }

    public string CategoryName { get; set; } = null!;

    public virtual ICollection<Answer> Answers { get; } = new List<Answer>();

    public virtual ICollection<RoundsCategory> RoundsCategories { get; } = new List<RoundsCategory>();

    public virtual ICollection<Word> Words { get; } = new List<Word>();
}
