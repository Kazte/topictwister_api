﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api.Domain.Models;

public partial class Round
{
    public int IdRound { get; set; }

    public int IdMatch { get; set; }

    public int RoundNumber { get; set; }

    public int IdLetter { get; set; }

    public bool IsRoundClosed { get; set; }

    public string? IdUserWinner { get; set; }

    public virtual ICollection<Answer> Answers { get; } = new List<Answer>();

    public virtual Letter IdLetterNavigation { get; set; } = null!;

    public virtual Match IdMatchNavigation { get; set; } = null!;

    public virtual ICollection<RoundsCategory> RoundsCategories { get; } = new List<RoundsCategory>();
}
