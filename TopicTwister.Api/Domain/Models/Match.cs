﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api.Domain.Models;

public partial class Match
{
    public int IdMatch { get; set; }

    public string IdUserOne { get; set; } = null!;

    public string IdUserTwo { get; set; } = null!;

    public int? IdMatchStatus { get; set; }

    public bool IsClosed { get; set; }

    public int UserOneScore { get; set; }

    public int UserTwoScore { get; set; }

    public int CurrentRoundNumber { get; set; }

    public virtual MatchStatus? IdMatchStatusNavigation { get; set; }

    public virtual ICollection<Round> Rounds { get; } = new List<Round>();
}
