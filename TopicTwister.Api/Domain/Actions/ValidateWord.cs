﻿using Microsoft.IdentityModel.Tokens;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.Providers;

namespace TopicTwister.Api.Actions;

public class ValidateWord
{
    private readonly ITopicTwisterRepository _repository;

    public ValidateWord(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<bool> Execute(string category, string letter, string word)
    {
        category = category.ToLower();
        letter = letter.ToLower();
        word = word.ToLower();

        if (word.IsNullOrEmpty() || !word.StartsWith(letter))
            return false;

        return await ValidatorActionProvider.IsWordInCategory(_repository).Execute(category, word);
    }
}