﻿using Microsoft.EntityFrameworkCore;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Actions;

public class GetRandomLetter
{
    private readonly ITopicTwisterRepository _repository;

    public GetRandomLetter(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<Letter> Execute()
    {
        var letters = await _repository.GetLetters();
        var random = new Random();
        var randomIndex = random.Next(0, letters.Count);

        var randomLetter = letters[randomIndex];

        return randomLetter;
    }
}