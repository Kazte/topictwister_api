﻿namespace TopicTwister.Api.Actions;

public class IsAllRoundsClosed
{
    public async Task<bool> Execute(Domain.Models.Match match)
    {
        return match.Rounds.All(x => x.IsRoundClosed);
    }
}