﻿using Microsoft.EntityFrameworkCore;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Actions;

public class GetRandomCategory
{
    private readonly ITopicTwisterRepository _repository;

    public GetRandomCategory(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<Category> Execute()
    {
        var categories = await _repository.GetCategories();
        var random = new Random();
        var randomIndex = random.Next(0, categories.Count);

        var randomCategory = categories[randomIndex];

        return randomCategory;
    }
}