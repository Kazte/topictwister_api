﻿using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Actions;

public class GetCurrentRoundId
{
    private ITopicTwisterRepository _repository;

    public GetCurrentRoundId(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<int> Execute(Domain.Models.Match match)
    {
        var rounds = await _repository.GetRoundsByMatchId(match.IdMatch);
        var round = rounds.FirstOrDefault(r => r.RoundNumber == match.CurrentRoundNumber);

        if (round == null)
            return 0;

        return round.IdRound;
    }
}