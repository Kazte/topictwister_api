﻿using Microsoft.EntityFrameworkCore;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Actions;

public class IsWordInCategory
{
    private readonly ITopicTwisterRepository _repository;

    public IsWordInCategory(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<bool> Execute(string categoryName, string word)
    {
        categoryName = categoryName.ToLower();
        word = word.ToLower();

        var categoryId =
            ((await _repository.GetCategories()).FirstOrDefault(x => x.CategoryName.ToLower() == categoryName))
            .IdCategory;

        var words = (await _repository.GetWords()).Where(x => x.IdCategory == categoryId).ToList();

        return words.Select(x => x.Description.ToLower()).Any(x => x == word);
    }
}