﻿using TopicTwister.Api.Interfaces;
using TopicTwister.Api.Providers;

namespace TopicTwister.Api.Actions;

public class CheckWinner
{
    private readonly ITopicTwisterRepository _repository;

    public CheckWinner(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<Domain.Models.Match> Execute(Domain.Models.Match match)
    {
        int playerOneCorrectAnswers = 0;
        int playerTwoCorrectAnswers = 0;

        int currentRoundId = await RoundActionProvider.GetCurrentRoundId(_repository).Execute(match);

        var rounds = await _repository.GetRoundsByMatchId(match.IdMatch);
        var currentRound = rounds.FirstOrDefault(x => x.IdRound == currentRoundId);

        var answers = await _repository.GetAnswersByRoundId(currentRoundId);

        foreach (var currentRoundAnswer in answers)
        {
            if ((bool)currentRoundAnswer.IsRight)
            {
                if (currentRoundAnswer.IdUser == match.IdUserOne)
                {
                    playerOneCorrectAnswers++;
                }
                else
                {
                    playerTwoCorrectAnswers++;
                }
            }
        }

        if (playerOneCorrectAnswers > playerTwoCorrectAnswers)
        {
            currentRound.IdUserWinner = match.IdUserOne;
        }
        else if (playerOneCorrectAnswers < playerTwoCorrectAnswers)
        {
            currentRound.IdUserWinner = match.IdUserTwo;
        }
        else
        {
            currentRound.IdUserWinner = null;
        }

        if (currentRound.IdUserWinner != null)
        {
            if (currentRound.IdUserWinner == match.IdUserOne)
            {
                match.UserOneScore++;
            }
            else if (currentRound.IdUserWinner == match.IdUserTwo)
            {
                match.UserTwoScore++;
            }
        }

        return match;
    }
}