﻿using Microsoft.EntityFrameworkCore;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Actions;

public class GetCategories
{
    private readonly ITopicTwisterRepository _repository;

    public GetCategories(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<List<Category>> Execute()
    {
        return (await _repository.GetCategories()).OrderBy(x => x.IdCategory).ToList();
    }
}