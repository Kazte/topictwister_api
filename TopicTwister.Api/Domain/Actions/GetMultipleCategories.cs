﻿using Microsoft.EntityFrameworkCore;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Actions;

public class GetMultipleCategories
{
    private readonly ITopicTwisterRepository _repository;

    public GetMultipleCategories(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<List<Category>> Execute(int amount)
    {
        var categories = await _repository.GetCategories();
        var randomCategoriesSelected = new List<Category>();

        var random = new Random();

        while (randomCategoriesSelected.Count < amount && randomCategoriesSelected.Count < categories.Count)
        {
            var randomNumber = random.Next(0, categories.Count);
            var categorySelected = categories[randomNumber];

            if (!randomCategoriesSelected.Contains(categorySelected))
                randomCategoriesSelected.Add(categorySelected);
        }

        return randomCategoriesSelected;
    }
}