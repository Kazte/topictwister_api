﻿using Microsoft.EntityFrameworkCore;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;

namespace TopicTwister.Api.Actions;

public class GetLetters
{
    private readonly ITopicTwisterRepository _repository;

    public GetLetters(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<List<Letter>> Execute()
    {
        return (await _repository.GetLetters()).OrderBy(x => x.IdLetter).ToList();
    }
}