using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.Mappers;
using TopicTwister.Api.OutputObjects;
using TopicTwister.Api.Providers;
using TopicTwister.Api.Repositories;
using TopicTwister.Api.Services;
using TopicTwister.Api.Use_Cases;

namespace TopicTwister.Api.Match.Controllers
{
    [Route("api/[action]/[controller]")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private readonly ITopicTwisterRepository _repository;

        private readonly IUserService _userService;

        // TODO: Deberia venir de otro lado. Variables globales?
        private const int MAX_CATEGORIES_AMOUNT = 5;
        private const int MAX_ROUNDS_PER_MATCH = 3;

        public MatchController()
        {
            _repository = new TopicTwisterRepositorySqlServer(new TopicTwisterContext());
            _userService = new UserService();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<MatchOutput>))]
        public async Task<IActionResult> GetMatchesByUserId(string userId)
        {
            var matches = await _repository.GetMatchesByUserId(userId);

            var matchesOutput = new List<MatchOutput>();

            var mapper = new MatchMapper(_repository, _userService);

            foreach (var match in matches)
            {
                matchesOutput.Add(await mapper.Mapper(match));
            }

            return new OkObjectResult(matchesOutput);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MatchOutput))]
        public async Task<IActionResult> GetMatchByMatchId(int matchId)
        {
            var match = await _repository.GetMatchById(matchId);

            if (match == null)
                return NotFound();

            var mapper = new MatchMapper(_repository, _userService);

            return new OkObjectResult(await mapper.Mapper(match));
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MatchOutput))]
        public async Task<IActionResult> FindMatch(string userOneId)
        {
            var matchFound = await new FindMatchUseCase(_userService, _repository).FindMatchWithId(userOneId);

            if (matchFound == null)
            {
                return Ok(null);
            }

            var matchId = await _repository.AddMatch(matchFound);

            var roundsGenerated =
                await new RoundsGeneratorUseCase(_repository).GenerateRoundsByAmount(MAX_ROUNDS_PER_MATCH);

            var categoryGenerator = new CategoriesGeneratorUserCase(_repository);

            foreach (var round in roundsGenerated)
            {
                var categories = await categoryGenerator.GenerateCategories(MAX_CATEGORIES_AMOUNT);

                round.IdMatch = matchId;

                var roundId = await _repository.AddRound(round);

                foreach (var category in categories)
                {
                    var newRoundCategory = new RoundsCategory()
                    {
                        IdCategory = category.IdCategory,
                        IdRound = roundId
                    };

                    await _repository.AddRoundCategory(newRoundCategory);
                }
            }

            var match = await _repository.GetMatchById(matchId);

            var matchOutput = await new MatchMapper(_repository, _userService).Mapper(match);

            return new OkObjectResult(matchOutput);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MatchOutput))]
        public async Task<IActionResult> CheckFindMatchStatus(string userId)
        {
            var user = await _userService.GetUserById(userId);

            if (user.isFinding)
            {
                return NoContent();
            }

            await Task.Delay(300);

            var lastMatch = await _repository.GetLastMatchByUserId(userId);

            if (lastMatch == null)
                return NotFound();

            var matchOutput = await new MatchMapper(_repository, _userService).Mapper(lastMatch);

            return new OkObjectResult(matchOutput);
        }

        [HttpPut]
        public async Task<IActionResult> CloseRound(int matchId)
        {
            var match = await _repository.GetMatchById(matchId);
            var currentRoundNumber = match.CurrentRoundNumber;

            if (match == null)
                return NotFound();

            if (currentRoundNumber == 3)
            {
                var isAllRoundsClosed = match.CurrentRoundNumber == 3;
                if (isAllRoundsClosed)
                {
                    var endMatchUseCase = new EndMatchUseCase(_repository);
                    match = await endMatchUseCase.EndMatch(match.IdMatch);
                    await _repository.UpdateMatch(match);

                    // Obtener usuario ganador
                    string userWinnerId = "";

                    switch (match.IdMatchStatus)
                    {
                        case 3:
                            userWinnerId = match.IdUserOne;
                            break;
                        case 4:
                            userWinnerId = match.IdUserTwo;
                            break;
                    }

                    // Updatear usuario en api de users
                    if (!string.IsNullOrEmpty(userWinnerId))
                    {
                        var userWinner = await _userService.GetUserById(userWinnerId);

                        userWinner.wins += 1;
                        userWinner.coins += 10;

                        await _userService.UpdateUser(userWinner);
                    }

                    return Ok();
                }
            }


            var newMatch = match;
            newMatch.CurrentRoundNumber++;

            await _repository.UpdateMatch(newMatch);


            return Ok();
        }
    }
}