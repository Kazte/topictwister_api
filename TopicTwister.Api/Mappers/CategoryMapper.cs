﻿using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.OutputObjects;

namespace TopicTwister.Api.Mappers;

public class CategoryMapper
{
    private ITopicTwisterRepository _repository;

    public CategoryMapper(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public CategoryOutput Mapper(Category category)
    {
        var categoryOutput = new CategoryOutput();

        categoryOutput.IdCategory = category.IdCategory;
        categoryOutput.CategoryName = category.CategoryName;

        return categoryOutput;
    }
}