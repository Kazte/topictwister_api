﻿using TopicTwister.Api.Interfaces;
using TopicTwister.Api.OutputObjects;
using TopicTwister.Api.Services;

namespace TopicTwister.Api.Mappers;

public class MatchMapper
{
    private ITopicTwisterRepository _repository;
    private readonly IUserService _userService;

    public MatchMapper(ITopicTwisterRepository repository, IUserService userService)
    {
        _repository = repository;
        _userService = userService;
    }

    public async Task<MatchOutput> Mapper(Domain.Models.Match match)
    {
        var matchOutput = new MatchOutput();

        var roundMapper = new RoundMapper(_repository);
        var rounds = new List<RoundOutput>();

        var roundsByMatch = await _repository.GetRoundsByMatchId(match.IdMatch);

        foreach (var round in roundsByMatch)
        {
            rounds.Add(await roundMapper.Mapper(round));
        }


        matchOutput.MatchId = match.IdMatch;
        matchOutput.Rounds = rounds;
        matchOutput.MatchStatusId = match.IdMatchStatus.Value;
        matchOutput.PlayerOneScore = match.UserOneScore;
        matchOutput.PlayerTwoScore = match.UserTwoScore;
        matchOutput.IsClosed = match.IsClosed;


        matchOutput.UserOne = await _userService.GetUserById(match.IdUserOne);
        matchOutput.UserTwo = await _userService.GetUserById(match.IdUserTwo);

        matchOutput.CurrentRoundNumber = match.CurrentRoundNumber;

        return matchOutput;
    }
}