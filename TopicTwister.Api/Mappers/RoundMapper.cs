﻿using TopicTwister.Api.Actions;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.OutputObjects;

namespace TopicTwister.Api.Mappers;

public class RoundMapper
{
    private ITopicTwisterRepository _repository;

    public RoundMapper(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<RoundOutput> Mapper(Round round)
    {
        var roundOutput = new RoundOutput();

        var answers = new List<AnswerOutput>();

        var answerMapper = new AnswerMapper(_repository);

        foreach (var roundAnswer in await _repository.GetAnswersByRoundId(round.IdRound))
        {
            answers.Add(await answerMapper.Mapper(roundAnswer));
        }

        roundOutput.Answers = answers;
        roundOutput.Letter = (await _repository.GetLetterById(round.IdLetter)).Description;
        roundOutput.IdRound = round.IdRound;
        roundOutput.RoundNumber = round.RoundNumber;
        roundOutput.IsClosed = round.IsRoundClosed;

        var roundCategoryMapper = new RoundCategoryMapper(_repository);

        var roundsCategory = await _repository.GetRoundsCategoryByRoundId(round.IdRound);

        roundOutput.Categories = new List<RoundCategoryOutput>();
        foreach (var rc in roundsCategory)
        {
            roundOutput.Categories.Add(await roundCategoryMapper.Mapper(rc));
        }


        return roundOutput;
    }
}