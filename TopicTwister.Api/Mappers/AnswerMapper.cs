﻿using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.OutputObjects;

namespace TopicTwister.Api.Mappers;

public class AnswerMapper
{
    private ITopicTwisterRepository _repository;

    public AnswerMapper(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<AnswerOutput> Mapper(Answer answer)
    {
        var answerOutput = new AnswerOutput();

        answerOutput.Answer = answer.UserAnswer;
        answerOutput.IdAnswer = answer.IdAnswer;

        var categoryMapper = new CategoryMapper(_repository);
        
        answerOutput.Category = categoryMapper.Mapper(await _repository.GetCategoryById(answer.IdCategory));

        answerOutput.IsRight = answer.IsRight.GetValueOrDefault();
        answerOutput.UserId = answer.IdUser;

        return answerOutput;
    }
}