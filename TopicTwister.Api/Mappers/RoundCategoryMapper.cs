﻿using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.OutputObjects;

namespace TopicTwister.Api.Mappers;

public class RoundCategoryMapper
{
    private ITopicTwisterRepository _repository;

    public RoundCategoryMapper(ITopicTwisterRepository repository)
    {
        _repository = repository;
    }

    public async Task<RoundCategoryOutput> Mapper(RoundsCategory roundsCategory)
    {
        var roundCategoryOutput = new RoundCategoryOutput();

        roundCategoryOutput.IdRound = roundsCategory.IdRound.GetValueOrDefault();
        roundCategoryOutput.IdCategory = roundsCategory.IdCategory.GetValueOrDefault();

        var category = await _repository.GetCategoryById(roundsCategory.IdCategory);
        roundCategoryOutput.CategoryName = category.CategoryName;

        return roundCategoryOutput;
    }
}