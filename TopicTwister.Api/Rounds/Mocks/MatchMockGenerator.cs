﻿using TopicTwister.Api.Domain.Models;

namespace TopicTwister.Api.Rounds.Mocks;

public class MatchMockGenerator
{
    public Domain.Models.Match GenerateMockupMatch(List<Answer> answers = null)
    {
        var match = new Domain.Models.Match();

        match.IdUserOne = "1";
        match.IdUserTwo = "2";

        match.IdMatchStatus = 1;

        var roundOne = new Round();

        roundOne.IdRound = 1;
        roundOne.RoundNumber = 1;
        foreach (var answer in answers)
        {
            roundOne.Answers.Add(answer);
        }


        match.Rounds.Add(roundOne);
        match.CurrentRoundNumber = 1;
        match.IdMatch = 1;

        return match;
    }

    public Domain.Models.Match GenerateMockupMatch_TwoRounds()
    {
        var match = new Domain.Models.Match();

        match.Rounds.Add(new Round()
        {
            IdRound = 1,
            RoundNumber = 1
        });

        match.Rounds.Add(new Round()
        {
            IdRound = 2,
            RoundNumber = 2
        });

        match.CurrentRoundNumber = 2;

        return match;
    }

    public Domain.Models.Match GenerateMockupMatch_FullRounds_PlayerOneWin()
    {
        var match = new Domain.Models.Match();
        
        match.IdUserOne = "1";
        
        match.IdUserTwo = "2";

        match.Rounds.Add(new Round()
        {
            IdRound = 1,
            RoundNumber = 1,
            IsRoundClosed = true
        });

        match.Rounds.Add(new Round()
        {
            IdRound = 2,
            RoundNumber = 2,
            IsRoundClosed = true
        });

        match.Rounds.Add(new Round()
        {
            IdRound = 3,
            RoundNumber = 3,
            IsRoundClosed = true
        });

        match.UserOneScore = 2;
        match.UserTwoScore = 1;

        return match;
    }
    
    public Domain.Models.Match GenerateMockupMatch_FullRounds_PlayerTwoWin()
    {
        var match = new Domain.Models.Match();
        
        match.IdUserOne = "1";
        
        match.IdUserTwo = "2";

        match.Rounds.Add(new Round()
        {
            IdRound = 1,
            RoundNumber = 1,
            IsRoundClosed = true
        });

        match.Rounds.Add(new Round()
        {
            IdRound = 2,
            RoundNumber = 2,
            IsRoundClosed = true
        });

        match.Rounds.Add(new Round()
        {
            IdRound = 3,
            RoundNumber = 3,
            IsRoundClosed = true
        });

        match.UserOneScore = 1;
        match.UserTwoScore = 2;

        return match;
    }
    
    public Domain.Models.Match GenerateMockupMatch_FullRounds_Tie()
    {
        var match = new Domain.Models.Match();
        
        match.IdUserOne = "1";
        
        match.IdUserTwo = "2";

        match.Rounds.Add(new Round()
        {
            IdRound = 1,
            RoundNumber = 1,
            IsRoundClosed = true
        });

        match.Rounds.Add(new Round()
        {
            IdRound = 2,
            RoundNumber = 2,
            IsRoundClosed = true
        });

        match.Rounds.Add(new Round()
        {
            IdRound = 3,
            RoundNumber = 3,
            IsRoundClosed = true
        });

        match.UserOneScore = 1;
        match.UserTwoScore = 1;

        return match;
    }
}