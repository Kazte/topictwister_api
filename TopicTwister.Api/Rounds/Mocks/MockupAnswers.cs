﻿using TopicTwister.Api.Domain.Models;

namespace TopicTwister.Api.Rounds.Mocks;

public class MockupAnswers
{
    public List<Answer> GetAnswersPlayerOneWin()
    {
        var answers = new List<Answer>();

        answers.Add(new Answer()
        {
            UserAnswer = "Anana",
            IdUser = "1",
            IsRight = true,
            IdCategory = 1
        });
        answers.Add(new Answer()
        {
            UserAnswer = "Arma",
            IdUser = "1",
            IsRight = true,
            IdCategory = 2
        });
        answers.Add(new Answer()
        {
            UserAnswer = "Anana",
            IdUser = "2",
            IsRight = true,
            IdCategory = 1
        });

        return answers;
    }

    public List<Answer> GetAnswersPlayerTwoWin()
    {
        var answers = new List<Answer>();

        answers.Add(new Answer()
        {
            UserAnswer = "Anana",
            IdUser = "2",
            IsRight = true,
            IdCategory = 1
        });
        answers.Add(new Answer()
        {
            UserAnswer = "Arma",
            IdUser = "2",
            IsRight = true,
            IdCategory = 2
        });
        answers.Add(new Answer()
        {
            UserAnswer = "Anana",
            IdUser = "1",
            IsRight = true,
            IdCategory = 1
        });

        return answers;
    }

    public List<Answer> GetAnswersTie()
    {
        var answers = new List<Answer>();

        answers.Add(new Answer()
        {
            UserAnswer = "Anana",
            IdUser = "2",
            IsRight = true,
            IdCategory = 1
        });
        answers.Add(new Answer()
        {
            UserAnswer = "Anana",
            IdUser = "1",
            IsRight = true,
            IdCategory = 1
        });

        return answers;
    }
}