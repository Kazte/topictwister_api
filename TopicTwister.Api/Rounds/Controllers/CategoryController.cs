using Microsoft.AspNetCore.Mvc;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.Providers;
using TopicTwister.Api.Repositories;

namespace TopicTwister.Api.Rounds.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ITopicTwisterRepository _repository;
        
        public CategoryController()
        {
            _repository = new TopicTwisterRepositorySqlServer(new TopicTwisterContext());
        }
        
        [HttpGet()]
        public async Task<IActionResult> GetCategories(int amount)
        {
            var categories = await CategoryActionProvider.GetMultipleCategories(_repository).Execute(amount);

            if (categories.Count < 0)
                return NotFound();
            
            return Ok(categories);
        }

    }
}
