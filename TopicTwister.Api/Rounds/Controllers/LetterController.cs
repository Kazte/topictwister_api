using Microsoft.AspNetCore.Mvc;
using TopicTwister.Api.Domain.Models;
using TopicTwister.Api.Interfaces;
using TopicTwister.Api.Providers;
using TopicTwister.Api.Repositories;

namespace TopicTwister.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LetterController : ControllerBase
    {
        private readonly ITopicTwisterRepository _repository;

        public LetterController()
        {
            _repository = new TopicTwisterRepositorySqlServer(new TopicTwisterContext());
        }

        [HttpGet]
        public async Task<IActionResult> GetLetter()
        {
            var letter = await LetterActionProvider.GetRandomLetter(_repository).Execute();

            if (letter == null)
                NotFound();

            return Ok(letter);
        }
    }
}