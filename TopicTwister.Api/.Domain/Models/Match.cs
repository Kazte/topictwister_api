﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api..Domain.Models;

public partial class Match
{
    public int IdMatch { get; set; }

    public int? IdUserOne { get; set; }

    public int? IdUserTwo { get; set; }

    public int? IdMatchStatus { get; set; }

    public bool? IsClosed { get; set; }

    public int UserOneScore { get; set; }

    public int UserTwoScore { get; set; }

    public virtual MatchStatus? IdMatchStatusNavigation { get; set; }

    public virtual User? IdUserOneNavigation { get; set; }

    public virtual User? IdUserTwoNavigation { get; set; }

    public virtual ICollection<Round> Rounds { get; } = new List<Round>();
}
