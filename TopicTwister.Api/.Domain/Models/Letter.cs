﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api..Domain.Models;

public partial class Letter
{
    public int IdLetter { get; set; }

    public string Description { get; set; } = null!;

    public virtual ICollection<Round> Rounds { get; } = new List<Round>();
}
