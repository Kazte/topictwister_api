﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace TopicTwister.Api..Domain.Models;

public partial class TopicTwisterContext : DbContext
{
    public TopicTwisterContext()
    {
    }

    public TopicTwisterContext(DbContextOptions<TopicTwisterContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Answer> Answers { get; set; }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Letter> Letters { get; set; }

    public virtual DbSet<Match> Matches { get; set; }

    public virtual DbSet<MatchStatus> MatchStatuses { get; set; }

    public virtual DbSet<Round> Rounds { get; set; }

    public virtual DbSet<RoundsCategory> RoundsCategories { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<Word> Words { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("server=DESKTOP-T1EEL9U\\SQLEXPRESS; database=TopicTwister; integrated security=true;MultipleActiveResultSets=True;trustServerCertificate=true");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Answer>(entity =>
        {
            entity.HasKey(e => e.IdAnswer).HasName("PK__Answers__A9B6978EEFE97FB2");

            entity.Property(e => e.IdAnswer).HasColumnName("idAnswer");
            entity.Property(e => e.IdCategory).HasColumnName("idCategory");
            entity.Property(e => e.IdRound).HasColumnName("idRound");
            entity.Property(e => e.IdUser).HasColumnName("idUser");
            entity.Property(e => e.IsRight).HasColumnName("isRight");
            entity.Property(e => e.UserAnswer)
                .HasMaxLength(255)
                .HasColumnName("userAnswer");

            entity.HasOne(d => d.IdCategoryNavigation).WithMany(p => p.Answers)
                .HasForeignKey(d => d.IdCategory)
                .HasConstraintName("FK_Answers_Categories");

            entity.HasOne(d => d.IdRoundNavigation).WithMany(p => p.Answers)
                .HasForeignKey(d => d.IdRound)
                .HasConstraintName("FK_Answers_Rounds");

            entity.HasOne(d => d.IdUserNavigation).WithMany(p => p.Answers)
                .HasForeignKey(d => d.IdUser)
                .HasConstraintName("FK_Answers_Users");
        });

        modelBuilder.Entity<Category>(entity =>
        {
            entity.HasKey(e => e.IdCategory).HasName("PK__Categori__79D361B66F7B3CD4");

            entity.Property(e => e.IdCategory).HasColumnName("idCategory");
            entity.Property(e => e.CategoryName)
                .HasMaxLength(255)
                .HasColumnName("categoryName");
        });

        modelBuilder.Entity<Letter>(entity =>
        {
            entity.HasKey(e => e.IdLetter);

            entity.Property(e => e.IdLetter).HasColumnName("idLetter");
            entity.Property(e => e.Description)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("description");
        });

        modelBuilder.Entity<Match>(entity =>
        {
            entity.HasKey(e => e.IdMatch).HasName("PK__Matches__044BF56BBC8E0C07");

            entity.Property(e => e.IdMatch).HasColumnName("idMatch");
            entity.Property(e => e.IdMatchStatus).HasColumnName("idMatchStatus");
            entity.Property(e => e.IdUserOne).HasColumnName("idUserOne");
            entity.Property(e => e.IdUserTwo).HasColumnName("idUserTwo");
            entity.Property(e => e.IsClosed).HasColumnName("isClosed");
            entity.Property(e => e.UserOneScore).HasColumnName("userOneScore");
            entity.Property(e => e.UserTwoScore).HasColumnName("userTwoScore");

            entity.HasOne(d => d.IdMatchStatusNavigation).WithMany(p => p.Matches)
                .HasForeignKey(d => d.IdMatchStatus)
                .HasConstraintName("FK_Matches_MatchStatus");

            entity.HasOne(d => d.IdUserOneNavigation).WithMany(p => p.MatchIdUserOneNavigations)
                .HasForeignKey(d => d.IdUserOne)
                .HasConstraintName("FK_Matches_Users");

            entity.HasOne(d => d.IdUserTwoNavigation).WithMany(p => p.MatchIdUserTwoNavigations)
                .HasForeignKey(d => d.IdUserTwo)
                .HasConstraintName("FK_Matches_Users1");
        });

        modelBuilder.Entity<MatchStatus>(entity =>
        {
            entity.HasKey(e => e.IdMatchStatus).HasName("PK__MatchSta__317258332E89853D");

            entity.ToTable("MatchStatus");

            entity.Property(e => e.IdMatchStatus).HasColumnName("idMatchStatus");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
        });

        modelBuilder.Entity<Round>(entity =>
        {
            entity.HasKey(e => e.IdRound).HasName("PK__Rounds__F6C717FD2BB61F24");

            entity.Property(e => e.IdRound).HasColumnName("idRound");
            entity.Property(e => e.IdLetter).HasColumnName("idLetter");
            entity.Property(e => e.IdMatch).HasColumnName("idMatch");
            entity.Property(e => e.IdUserWinner).HasColumnName("idUserWinner");
            entity.Property(e => e.IsRoundClosed).HasColumnName("isRoundClosed");
            entity.Property(e => e.RoundNumber).HasColumnName("roundNumber");

            entity.HasOne(d => d.IdLetterNavigation).WithMany(p => p.Rounds)
                .HasForeignKey(d => d.IdLetter)
                .HasConstraintName("FK_Rounds_Letters");

            entity.HasOne(d => d.IdMatchNavigation).WithMany(p => p.Rounds)
                .HasForeignKey(d => d.IdMatch)
                .HasConstraintName("FK_Rounds_Matches");

            entity.HasOne(d => d.IdUserWinnerNavigation).WithMany(p => p.Rounds)
                .HasForeignKey(d => d.IdUserWinner)
                .HasConstraintName("FK_Rounds_Users");
        });

        modelBuilder.Entity<RoundsCategory>(entity =>
        {
            entity.HasKey(e => e.IdRoundCategory).HasName("PK__RoundsCa__1D9AA1498328B774");

            entity.ToTable("RoundsCategory");

            entity.Property(e => e.IdRoundCategory).HasColumnName("idRoundCategory");
            entity.Property(e => e.IdCategory).HasColumnName("idCategory");
            entity.Property(e => e.IdRound).HasColumnName("idRound");

            entity.HasOne(d => d.IdCategoryNavigation).WithMany(p => p.RoundsCategories)
                .HasForeignKey(d => d.IdCategory)
                .HasConstraintName("FK_RoundsCategory_Categories");

            entity.HasOne(d => d.IdRoundNavigation).WithMany(p => p.RoundsCategories)
                .HasForeignKey(d => d.IdRound)
                .HasConstraintName("FK_RoundsCategory_Rounds");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.IdUser).HasName("PK__Users__3717C98263C6B360");

            entity.Property(e => e.IdUser).HasColumnName("idUser");
            entity.Property(e => e.Email)
                .HasMaxLength(50)
                .HasColumnName("email");
            entity.Property(e => e.IsFinding).HasColumnName("isFinding");
            entity.Property(e => e.Password)
                .HasMaxLength(50)
                .HasColumnName("password");
            entity.Property(e => e.Username)
                .HasMaxLength(255)
                .HasColumnName("username");
            entity.Property(e => e.Wins).HasColumnName("wins");
        });

        modelBuilder.Entity<Word>(entity =>
        {
            entity.HasKey(e => e.IdWord).HasName("PK__Words__15C3E27DAC10C8DD");

            entity.Property(e => e.IdWord).HasColumnName("idWord");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
            entity.Property(e => e.IdCategory).HasColumnName("idCategory");

            entity.HasOne(d => d.IdCategoryNavigation).WithMany(p => p.Words)
                .HasForeignKey(d => d.IdCategory)
                .HasConstraintName("FK_Words_Categories");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
