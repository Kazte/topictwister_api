﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api..Domain.Models;

public partial class Word
{
    public int IdWord { get; set; }

    public string? Description { get; set; }

    public int? IdCategory { get; set; }

    public virtual Category? IdCategoryNavigation { get; set; }
}
