﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api..Domain.Models;

public partial class RoundsCategory
{
    public int IdRoundCategory { get; set; }

    public int? IdRound { get; set; }

    public int? IdCategory { get; set; }

    public virtual Category? IdCategoryNavigation { get; set; }

    public virtual Round? IdRoundNavigation { get; set; }
}
