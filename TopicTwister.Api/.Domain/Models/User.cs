﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api..Domain.Models;

public partial class User
{
    public int IdUser { get; set; }

    public string? Username { get; set; }

    public bool IsFinding { get; set; }

    public int Wins { get; set; }

    public string? Password { get; set; }

    public string? Email { get; set; }

    public virtual ICollection<Answer> Answers { get; } = new List<Answer>();

    public virtual ICollection<Match> MatchIdUserOneNavigations { get; } = new List<Match>();

    public virtual ICollection<Match> MatchIdUserTwoNavigations { get; } = new List<Match>();

    public virtual ICollection<Round> Rounds { get; } = new List<Round>();
}
