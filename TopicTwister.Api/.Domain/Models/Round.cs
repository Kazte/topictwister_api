﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api..Domain.Models;

public partial class Round
{
    public int IdRound { get; set; }

    public int? IdMatch { get; set; }

    public int? RoundNumber { get; set; }

    public int? IdLetter { get; set; }

    public bool IsRoundClosed { get; set; }

    public int? IdUserWinner { get; set; }

    public virtual ICollection<Answer> Answers { get; } = new List<Answer>();

    public virtual Letter? IdLetterNavigation { get; set; }

    public virtual Match? IdMatchNavigation { get; set; }

    public virtual User? IdUserWinnerNavigation { get; set; }

    public virtual ICollection<RoundsCategory> RoundsCategories { get; } = new List<RoundsCategory>();
}
