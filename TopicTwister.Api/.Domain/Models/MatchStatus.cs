﻿using System;
using System.Collections.Generic;

namespace TopicTwister.Api..Domain.Models;

public partial class MatchStatus
{
    public int IdMatchStatus { get; set; }

    public string? Description { get; set; }

    public virtual ICollection<Match> Matches { get; } = new List<Match>();
}
