﻿using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using TopicTwister.Api.OutputObjects;

namespace TopicTwister.Api.Services;

public class UserService : IUserService
{
    private readonly HttpClient _client;

    private string baseUrl = StringConnections.UserApiUrl;

    public UserService()
    {
        _client = new HttpClient();
    }

    public async Task<List<UserOutput>> GetUsers()
    {
        using (var request = new HttpRequestMessage(new HttpMethod("GET"), $"{baseUrl}/users"))
        {
            request.Headers.TryAddWithoutValidation("accept", "application/json");

            var response = await _client.SendAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var contentResult = await response.Content.ReadAsStringAsync();

                var convertedToList = JsonConvert.DeserializeObject<List<UserOutput>>(contentResult);

                return convertedToList;
            }
        }

        throw new Exception("No se pudo conectar");
    }

    public async Task<UserOutput> GetUserById(string userId)
    {
        using (var request = new HttpRequestMessage(new HttpMethod("GET"), $"{baseUrl}/users/{userId}"))
        {
            request.Headers.TryAddWithoutValidation("accept", "application/json");

            var response = await _client.SendAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var contentResult = await response.Content.ReadAsStringAsync();

                var convertedToList = JsonConvert.DeserializeObject<UserOutput>(contentResult);

                return convertedToList;
            }
        }

        throw new Exception("No se pudo conectar");
    }

    public async Task UpdateUser(UserOutput userOutput)
    {
        using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"{baseUrl}/update-user"))
        {
            request.Headers.TryAddWithoutValidation("accept", "application/json");

            var user = userOutput;

            var jsonUser = JsonConvert.SerializeObject(user);

            request.Content =
                new StringContent(jsonUser);
            request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

            var response = await _client.SendAsync(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }
    }
}

public interface IUserService
{
    Task<List<UserOutput>> GetUsers();
    Task<UserOutput> GetUserById(string userId);
    Task UpdateUser(UserOutput userOutput);
}