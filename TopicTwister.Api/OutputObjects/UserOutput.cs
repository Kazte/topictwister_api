﻿namespace TopicTwister.Api.OutputObjects;

public class UserOutput
{
    public string userId { get; set; }
    public string email { get; set; }
    public string username { get; set; }
    public int wins { get; set; }
    public bool isFinding { get; set; }
    public bool isLogged { get; set; }
    public int coins { get; set; }
}