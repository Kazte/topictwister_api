﻿namespace TopicTwister.Api.OutputObjects;

public class AnswerOutput
{
    public int IdAnswer { get; set; }
    public CategoryOutput Category { get; set; }
    public string Answer { get; set; }
    public string UserId { get; set; }
    public bool IsRight { get; set; }
}