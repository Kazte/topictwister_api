﻿namespace TopicTwister.Api.OutputObjects;

public class FindingStatusOutput
{
    public int FindingStatusCode { get; set; }
    public Domain.Models.Match Match { get; set; }
}