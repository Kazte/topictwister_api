﻿namespace TopicTwister.Api.OutputObjects;

public class CategoryOutput
{
    public int IdCategory { get; set; }
    public string CategoryName { get; set; }
}

