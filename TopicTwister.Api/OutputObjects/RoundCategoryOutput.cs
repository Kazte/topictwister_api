﻿namespace TopicTwister.Api.OutputObjects;

public class RoundCategoryOutput
{
    public int IdRound { get; set; }
    public int IdCategory { get; set; }
    public string CategoryName { get; set; }
}