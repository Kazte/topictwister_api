﻿using TopicTwister.Api.Domain.Models;

namespace TopicTwister.Api.OutputObjects;

public class MatchOutput
{
    public int MatchId { get; set; }
    public List<RoundOutput> Rounds { get; set; }
    public int PlayerOneScore { get; set; }
    public int PlayerTwoScore { get; set; }
    public int MatchStatusId { get; set; }
    public UserOutput UserOne { get; set; }
    public UserOutput UserTwo { get; set; }
    public int CurrentRoundNumber { get; set; }
    public bool IsClosed { get; set; }
}