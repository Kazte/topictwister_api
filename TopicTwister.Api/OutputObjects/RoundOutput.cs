﻿namespace TopicTwister.Api.OutputObjects;

public class RoundOutput
{
    public int IdRound { get; set; }
    public int RoundNumber { get; set; }
    public string Letter { get; set; }
    public List<AnswerOutput> Answers { get; set; }
    public List<RoundCategoryOutput> Categories { get; set; }
    public bool IsClosed { get; set; }
}